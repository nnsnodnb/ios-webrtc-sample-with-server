# ios-webrtc-sample-with-server

## Environment

- Xcode 10.1
- Carthage 0.32.0
- Docker 18.09.2

## Setup

```bash
$ git clone https://nnsnodnb@bitbucket.org/nnsnodnb/ios-webrtc-sample-with-server.git /path/to/ios-webrtc-sample-with-server
$ cd /path/to/ios-webrtc-sample-with-server
$ carthage bootstrap --platform iOS
$ docker build websocket-server .
$ docker run --rm -d -p 8001:8080 websocket-server:latest
```

## Acknowledgment

- https://qiita.com/tnoho/items/f5afa3ba749eed9b9716

## Demo Screenshot

![img_0729](https://user-images.githubusercontent.com/9856514/54008090-6406f700-41a8-11e9-897f-77c370af440e.jpg)
