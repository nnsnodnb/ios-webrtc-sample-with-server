FROM golang:1.12.0

WORKDIR /go/src/github.com

RUN go get github.com/gorilla/websocket
RUN go get github.com/comail/colog
RUN go get github.com/gorilla/mux

RUN git clone https://github.com/tnoho/WebRTCHandsOnSig.git
WORKDIR /go/src/github.com/WebRTCHandsOnSig

EXPOSE 8080

CMD ["go", "run", "main.go", "room.go", "client.go"]
