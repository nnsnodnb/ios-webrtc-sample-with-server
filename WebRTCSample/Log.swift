//
//  Log.swift
//  WebRTCSample
//
//  Created by Yuya Oka on 2019/03/07.
//  Copyright © 2019 nnsnodnb. All rights reserved.
//

import Foundation

func print(_ object: Any, function: String = #function, line: Int = #line) {
    Swift.print("[\(function): \(line)] \(object)")
}
