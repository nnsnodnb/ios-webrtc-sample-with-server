//
//  ViewController.swift
//  WebRTCSample
//
//  Created by Yuya Oka on 2019/03/07.
//  Copyright © 2019 nnsnodnb. All rights reserved.
//

import UIKit
import Starscream
import SwiftyJSON
import WebRTC

final class ViewController: UIViewController {

    @IBOutlet private weak var remoteVideoView: RTCEAGLVideoView! {
        didSet {
            remoteVideoView.delegate = self
        }
    }
    @IBOutlet private weak var cameraPreviewView: RTCCameraPreviewView!

    private var websocket: WebSocket!
    private var peerConnectionFactory: RTCPeerConnectionFactory!
    private var peerConnection: RTCPeerConnection!
    private var audioSource: RTCAudioSource?
    private var videoSource: RTCAVFoundationVideoSource?
    private var remoteVideoTrack: RTCVideoTrack?

    // MARK: - Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        peerConnectionFactory = RTCPeerConnectionFactory()
        startVideo()
        connectWebsocket()
    }

    deinit {
        if peerConnection != nil {
            hangup()
        }
        audioSource = nil
        videoSource = nil
        peerConnectionFactory = nil
    }

    // MARK: - Private method

    private func connectWebsocket() {
        websocket = WebSocket(url: URL(string: "ws://10.0.1.9:8001/WebRTCHandsOnSig/nnsnodnb")!)
        websocket.delegate = self
        websocket.connect()
    }

    private func startVideo() {
        let audioSourceConstraints = RTCMediaConstraints(mandatoryConstraints: nil, optionalConstraints: nil)
        // 音声ソース生成
        audioSource = peerConnectionFactory.audioSource(with: audioSourceConstraints)
        let videoSourceConstraints = RTCMediaConstraints(mandatoryConstraints: nil, optionalConstraints: nil)
        videoSource = peerConnectionFactory.avFoundationVideoSource(with: videoSourceConstraints)

        cameraPreviewView.captureSession = videoSource?.captureSession
    }

    private func prepareNewConnection() -> RTCPeerConnection {
        let configuration = RTCConfiguration()
        configuration.iceServers = [
            RTCIceServer(urlStrings: ["stun:stun.l.google.com:19302"])
        ]
        let peerConnectionConstraints = RTCMediaConstraints(mandatoryConstraints: nil, optionalConstraints: nil)
        let connection = peerConnectionFactory.peerConnection(with: configuration, constraints: peerConnectionConstraints, delegate: self)

        if let audioSource = audioSource {
            let localAudioTrack = peerConnectionFactory.audioTrack(with: audioSource, trackId: "ARDAMSa0")
            let audioSender = connection.sender(withKind: kRTCMediaStreamTrackKindAudio, streamId: "ARDAMS")
            audioSender.track = localAudioTrack
        }

        if let videoSource = videoSource {
            let localVideoTrack = peerConnectionFactory.videoTrack(with: videoSource, trackId: "ARDAMSv0")
            // PeerConnectionからVideoのSenderを作成
            let videoSender = connection.sender(withKind: kRTCMediaStreamTrackKindVideo, streamId: "ARDAMS")
            // Senderにトラックを設定
            videoSender.track = localVideoTrack
        }

        return connection
    }

    private func hangup() {
        if peerConnection != nil {
            if peerConnection.iceConnectionState != RTCIceConnectionState.closed {
                peerConnection.close()
                let jsonClose: JSON = [
                    "type": "close"
                ]
                print("sending close message")
                websocket.write(string: jsonClose.rawString()!)
            }
            if remoteVideoTrack != nil {
                remoteVideoTrack?.remove(remoteVideoView)
            }
            remoteVideoTrack = nil
            peerConnection = nil
            print("peerConnection is closed.")
        }
    }

    private func sendSDP(_ desc: RTCSessionDescription) {
        print("---- sending SDP ----")
        let jsonSDP: JSON = [
            "sdp": desc.sdp, // SDP本体
            "type": RTCSessionDescription.string(for: desc.type) // offer か answer か
        ]
        // JSONを生成
        let message = jsonSDP.rawString()!
        print("sending SDP = " + message)
        // 相手に送信
        websocket.write(string: message)
    }

    private func makeOffer() {
        if peerConnection == nil {
            peerConnection = prepareNewConnection()
        }
        let constraints = RTCMediaConstraints(mandatoryConstraints: ["OfferToReceiveAudio": "true", "OfferToReceiveVideo": "true"],
                                              optionalConstraints: nil)

        peerConnection.offer(for: constraints) { [weak self] (offer, error) in
            guard let wself = self, let offer = offer, error == nil else {
                print(error!)
                return
            }

            wself.peerConnection.setLocalDescription(offer) { [weak self] (error) in
                guard let wself = self, error == nil else {
                    print(error!)
                    return
                }
                print("Success")
                // 相手に送る
                wself.sendSDP(offer)
            }
        }
    }

    private func setAnswer(_ answer: RTCSessionDescription) {
        if peerConnection == nil {
            print("PeerConnection NOT exist!")
            return
        }
        peerConnection.setRemoteDescription(answer) { (error) in
            if let error = error {
                print(error)
            } else {
                print("Success")
            }
        }
    }

    private func sendIceCandidate(_ candidate: RTCIceCandidate) {
        print("---- Sending ICE candidate ----")
        let jsonCandidate: JSON = [
            "type": "candidate",
            "ice": [
                "candidate": candidate.sdp,
                "sdpMLineIndex": candidate.sdpMLineIndex,
                "sdpMid": candidate.sdpMid!
            ]
        ]
        let message = jsonCandidate.rawString()!
        print("sending candidate = " + message)
        websocket.write(string: message)
    }

    private func addIceCandidate(_ candidate: RTCIceCandidate) {
        if peerConnection != nil {
            peerConnection.add(candidate)
        } else {
            print("PeerConnection not exist!")
        }
    }

    private func setOffer(_ offer: RTCSessionDescription) {
        if peerConnection != nil {
            print("peerConnection alreay exist!")
        }
        // PeerConnectionを生成する
        peerConnection = prepareNewConnection()
        peerConnection.setRemoteDescription(offer) { [weak self] (error) in
            if error == nil {
                print("Succsess")
                self?.makeAnswer()
            } else {
                print("ERROR: \(error!)")
            }
        }
    }

    private func makeAnswer() {
        print("sending Answer. Creating remote session description...")
        if peerConnection == nil {
            print("peerConnection NOT exist!")
            return
        }
        let constraints = RTCMediaConstraints(mandatoryConstraints: nil, optionalConstraints: nil)
        // Answerを生成
        peerConnection.answer(for: constraints) { [weak self] (answer, error) in
            if error != nil {
                print(error!)
                return
            }
            print("Success")
            self?.peerConnection.setLocalDescription(answer!) { (error) in
                if error != nil {
                    print(error!)
                    return
                }
                // 相手に送る
                self?.sendSDP(answer!)
            }

        }
    }

    // MARK: - IBAction

    @IBAction func onTapConnectButton(_ sender: UIButton) {
        if peerConnection == nil {
            makeOffer()
        } else {
            print("Peer already exist.")
        }
        if websocket.isConnected { return }
        connectWebsocket()
    }

    @IBAction func onTapHangUpButton(_ sender: UIButton) {
        hangup()
    }
}

// MARK: - WebSocketDelegate

extension ViewController: WebSocketDelegate {

    func websocketDidConnect(socket: WebSocketClient) {
        print("繋がった！")
    }

    func websocketDidDisconnect(socket: WebSocketClient, error: Error?) {
        print("終了")
    }

    func websocketDidReceiveMessage(socket: WebSocketClient, text: String) {
        print("受け取った: \(text)")
        let jsonMessage = JSON(parseJSON: text)
        let type = jsonMessage["type"].stringValue
        switch type {
        case "answer":
            print("Received answer...")
            let answer = RTCSessionDescription(type: RTCSessionDescription.type(for: type), sdp: jsonMessage["sdp"].stringValue)
            setAnswer(answer)
        case "candidate":
            print("Received ICE candidate ...")
            let candidate = RTCIceCandidate(
                sdp: jsonMessage["ice"]["candidate"].stringValue,
                sdpMLineIndex:
                jsonMessage["ice"]["sdpMLineIndex"].int32Value,
                sdpMid: jsonMessage["ice"]["sdpMid"].stringValue)
            addIceCandidate(candidate)
        case "offer":
            // offerを受け取った時の処理
            print("Received offer ...")
            let offer = RTCSessionDescription(type: RTCSessionDescription.type(for: type), sdp: jsonMessage["sdp"].stringValue)
            setOffer(offer)
        default:
            return
        }
    }

    func websocketDidReceiveData(socket: WebSocketClient, data: Data) {
        print("データを受け取った")
    }
}

// MARK: - RTCPeerConnectionDelegate

extension ViewController: RTCPeerConnectionDelegate {

    func peerConnection(_ peerConnection: RTCPeerConnection, didChange stateChanged: RTCSignalingState) {

    }

    func peerConnection(_ peerConnection: RTCPeerConnection, didAdd stream: RTCMediaStream) {
        print("-- Peer onaddstream")
        DispatchQueue.main.async {
            // 映像/音声が追加された際に呼ばれる
            if stream.videoTracks.count < 0 { return }
            self.remoteVideoTrack = stream.videoTracks[0]
            self.remoteVideoTrack?.add(self.remoteVideoView)
        }
    }

    func peerConnection(_ peerConnection: RTCPeerConnection, didRemove stream: RTCMediaStream) {

    }

    func peerConnectionShouldNegotiate(_ peerConnection: RTCPeerConnection) {

    }

    func peerConnection(_ peerConnection: RTCPeerConnection, didChange newState: RTCIceGatheringState) {

    }

    func peerConnection(_ peerConnection: RTCPeerConnection, didChange newState: RTCIceConnectionState) {
        switch newState {
        case .checking:
            print("cheking")
        case .completed:
            print("completed")
        case .connected:
            print("connected")
        case .closed:
            print("closed")
            hangup()
        case .failed:
            print("failed")
            hangup()
        case .disconnected:
            print("disconnected")
        case .new:
            print("new")
        case .count:
            print("count")
        }
    }

    func peerConnection(_ peerConnection: RTCPeerConnection, didGenerate candidate: RTCIceCandidate) {
        if candidate.sdpMid != nil {
            sendIceCandidate(candidate)
        } else {
            print("Empty ice event")
        }
    }

    func peerConnection(_ peerConnection: RTCPeerConnection, didOpen dataChannel: RTCDataChannel) {

    }

    func peerConnection(_ peerConnection: RTCPeerConnection, didRemove candidates: [RTCIceCandidate]) {

    }
}

// MARK: - RTCEAGLVideoViewDelegate

extension ViewController: RTCEAGLVideoViewDelegate {

    func videoView(_ videoView: RTCEAGLVideoView, didChangeVideoSize size: CGSize) {
        let width = view.frame.width
        let height = view.frame.width * size.height / size.width
        videoView.frame = CGRect(x: 0, y: (view.frame.height - height) / 2, width: width, height: height)
    }
}
